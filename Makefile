#!/bin/bash
CC=mpicc
RM=rm -f

FILE_PREFIX=.c
OBJ=.o

SRC_PATH=src
INC_PATH=include

CFLAGS=-march=native -Wall $(MODE) -I$(INC_PATH) -fopenmp
LDFLAGS=-lpthread -lm

SOURCES=main.c opt_helper.c $(SRC_PATH)/statistics.c $(SRC_PATH)/file_utils.c \
		$(SRC_PATH)/mandelbrot.c $(SRC_PATH)/color_map.c $(SRC_PATH)/server.c \
		$(SRC_PATH)/stack.c computations.c
OBJECTS=$(SOURCES:.c=.o)

EXECUTABLE=mandela

MODE=$(RELEASE)

RELEASE=-O2
DEBUG=-g -D__YAL_ON__
DEBUG_QUIET=-g -rdynamic

all: $(SOURCES) $(EXECUTABLE)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)

$(EXECUTABLE):$(OBJECTS)

%$(OBJ): %(FILE_PREFIX)
	$(CC) $(CFLAGS) $< $@ $(LDFLAGS)

clean:
	$(RM) $(OBJECTS) $(EXECUTABLE)
