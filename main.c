#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>

#include <execinfo.h>
#include <signal.h>

#include "opt_helper.h"
#include "server.h"
#include "computations.h"

extern struct argp_option options[];
extern struct argp argp;
extern struct execution_info ei;

extern mpi_datatype eis_datatype;

extern void
(*compute)(byte*, unumber* hist, struct execution_info_slave*);

extern bool stop;

void
(*execute)(byte*, unumber* hist, struct execution_info_slave*);


struct execution_info_slave eis;

number comm_size;
number rank;

/* function created for debugging purposes */
void handler(number sig)
{
  void *array[100];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 100);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

int
main(number argc, char** argv)
{
  signal(SIGSEGV, handler);
  number dummy;
  mpi_init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &dummy);
  
  /* gets the size and rank of the current process */
  mpi_comm_size(MPI_COMM_WORLD, &comm_size);
  mpi_comm_rank(MPI_COMM_WORLD, &rank);

  /* creates the mpi required data types */
  create_mpi_mb_grid_type();
  create_mpi_execution_info_slave_type();

  /* starts measuring the total time */
  real beg = mpi_wtime();

  if(rank == 0)
  {
    /* parses the terminal arguments */
    number ret = argp_parse(&argp, argc, argv, 0, 0, &ei);
    if(ret)
    {
      printf("failed parsing arguments\n");
      exit(-1);
    }

    /* sets the desired statistic fields */
    set_statistics(
          ei.s_i.rt_avg, 
          ei.s_i.ut_avg, 
          ei.s_i.rt_v, 
          ei.s_i.ut_v, 
          ei.s_i.num_cpus, 
          ei.s_i.cpu_occupation,
          ei.s_i.speed_up);

    /* sets up the worker information */
    eis.join_output           = ei.join_output;
    eis.print_image           = ei.print_image;
    strcpy(eis.image_fn, ei.image_fn);
    eis.execution_type        = (ei.eval ? EXEC_EVAL : EXEC_RUN);
    eis.runs                  = ei.runs;
    eis.n_threads             = ei.n_threads;
    eis.data                  = ei.data;
    eis.histogram             = ei.histogram;
    eis.dynamic_parallelism   = ei.omp_dynamic;
    eis.execution_mode        = ei.execution_mode;
    show_exec_info_slave(&eis);

  }

  /* sends/gets broadcasted data */
  mpi_bcast(&eis, 1, eis_datatype, 0, MPI_COMM_WORLD);

  if(eis.execution_type == EXEC_EVAL)
  {
    if(eis.execution_mode == WS_MODE
       || eis.execution_mode == DYNAMIC_MODE)
    {
      printf("custom evaluation with a dynamic mode is not supported\n");
    }
  }

  /* sets the intra node computation mode */
  if(eis.dynamic_parallelism)
  {
    /* dynamic parallelism */
    set_computation_type(dynamic_comp);
  }
  else
  {
    /* static parallelism */
    set_computation_type(static_comp);
  }

  /* the height and width of each part */
  unumber width = eis.data.width;
  unumber height = eis.data.len / comm_size;

  /* sets up a buffer for the mandelbrot computation */
  byte* b;
  if(rank == 0)
  {
    if(eis.join_output)
    {
      b = (byte*) malloc(width * eis.data.len * sizeof(byte));
    }
    else
    {
      b = (byte*) malloc(width * height * sizeof(byte));
    }
  }
  else
  {
    b = (byte*) malloc(width * height * sizeof(byte));
  }

  /* sets the number of threads to execute in parallel */
  set_num_threads(eis.n_threads);

  /* sets the inter node execution mode */
  switch(eis.execution_mode)
  {
    case STATIC_MODE:
    {
      /* each node computes a fixed part */
      compute = compute_static;
      break;
    }
    case DYNAMIC_MODE:
    {
      /* each node keeps computing and 
       *  fetching tasks from the server */
      compute = compute_dynamic;
      break;
    }
    case WS_MODE:
    {
      /* each node keeps computing and
       *  fetching tasks from random peers */
      compute = compute_dynamic;
      break;
    }
    default:
    {
      eis.execution_mode = WS_MODE;
      compute = compute_dynamic;
      break;
    }
  }

  /* initializes communicators */
  init_lb_comm();
  init_cp_comm();

  /* sets the execution type function */
  switch(eis.execution_type)
  {
    case EXEC_RUN:
    {
      execute = execute_run;
      break;
    }
    case EXEC_EVAL:
    {
      execute = execute_eval;
      break;
    }
    default:
    {
      execute = execute_run;
      break;
    }
  }

  init_structures();

  unumber* hist = NULL;

  if(eis.histogram)
  {
    hist = (unumber*) calloc(NUM_COLOR_VALUES, sizeof(unumber));
  }

  mpi_barrier(MPI_COMM_WORLD);

  /* executes */
  execute(b, hist, &eis);

  if(!rank)
  {
    if(eis.histogram)
    {
      if(ei.histogram_print)
      {
        write_hist(stdout, hist);
      }

      FILE* f = fopen(ei.histogram_fn, "w");
      assert(f);
      write_hist(f, hist);
      fclose(f);

      /* show */
      free(hist);
    }
  }
  
  destroy_structures();

  /* prints the final output image */
  if(eis.print_image)
  {
    /* only the master prints the total image */
    if(eis.join_output)
    {
      if(rank == 0)
      {
        /* prints the image */
        write_pgm(eis.image_fn, b, ei.data.width, ei.data.len, MAX_ITERATIONS);
      }
    }
    else
    {
      /* each node prints his part of the image */
      /* print the image */
      char output_name[MAX_FILENAME_SIZE];
      sprintf(output_name, UNUM".%s", rank, eis.image_fn);
      write_pgm(output_name, b, width, height, MAX_ITERATIONS);
    }
  }

  /* frees the buffer */
  free(b);

  /* waits for the end of the computation */
  mpi_barrier(MPI_COMM_WORLD);

  /* marks the end of the computation */
  real end = mpi_wtime();
  real time = end - beg;

  /* shows the total time taken */
  if(ei.show_total_time)
  {
    printf("\ttotal time:" REAL "\n", time);
  }

  /* destroys the communicators created */
  destroy_lb_comm();
  destroy_cp_comm();

  /* destroys the created mpi data types */
  destroy_mpi_execution_info_slave_type();
  destroy_mpi_mb_grid_type();

  /* finalizes the mpi environment */
  mpi_finalize();

  return 0;
}
