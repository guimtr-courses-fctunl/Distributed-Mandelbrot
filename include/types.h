#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "mympi.h"

#ifndef __DATA_TYPES__
#define __DATA_TYPES__

#define MAX_FILENAME_SIZE 1024

/* general types */
#define BOOL 				"%d"
typedef bool				boolean;

#define mpi_byte_t 			MPI_UNSIGNED_CHAR
#define BYTE 				"%d"
typedef uint8_t 			byte;

#define mpi_number_t		MPI_INT
#define NUM 				"%d"
typedef int32_t 			number;

#define mpi_unumber_t		MPI_UNSIGNED
#define UNUM 				"%u"
typedef uint32_t			unumber;

#define mpi_real_t			MPI_DOUBLE
#define REAL 				"%lf"
typedef double				real;

#define mpi_char_t 			MPI_CHAR

/* color type information */

#define COLOR_NR_FIELDS 	3
#define NUM_COLOR_VALUES 	256

#define COLOR_DEPTH			1

#define COLOR 				"%d"
typedef uint8_t 			color_t;


/* execution type information */
#define EXEC_RUN			0x00
#define EXEC_EVAL			0x01

/* parallelization types */
#define PAR_SEQ				0x00
#define PAR_OMP				0x01


#endif /* __DATA_TYPES__ */