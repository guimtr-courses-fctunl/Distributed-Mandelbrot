#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <unistd.h>

#include "stack.h"
#include "config.h"
#include "logger.h"

#ifndef __SERVER_LOAD_BALANCER__
#define __SERVER_LOAD_BALANCER__

#define REQUEST_WORK_TAG		0x0015
#define WORK_TAG				0x0016
#define INVALID_REQUEST_TAG		0x0017


#define JOIN_TAG				0x0021

/* creates the load balancing comm */
void
init_lb_comm();

/* destroys the load balancing comm */
void
destroy_lb_comm();

/* creates the checkpoint comm */
void
init_cp_comm();

/* destroys the checkpoint comm */
void
destroy_cp_comm();

/* the load balancer daemon function */
void*
deamon(void*);

/* the checkpoint function */
void*
checkpoint(void*);

#endif  /* __SERVER_LOAD_BALANCER__ */