#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <mpi.h>
#include <omp.h>

#include "config.h"
#include "types.h"
#include "mympi.h"
#include "logger.h"

#ifndef __MANDELBROT__
#define __MANDELBROT__



#define MAX_ITERATIONS (255)

#define NUM_FIELDS_MB_GRID 6
#define NUM_FIELDS_OUTPUT_GRID 2



/* defines the mandelbrot working set */
struct mb_grid
{
	real   xi;
	real   xf;
	real   yi;
	real   yf;
	unumber width;
	unumber len;
};

/* defines the mandelbrot ouput part */
struct part_output_grid
{
	unumber offset;
	byte* b;
};

typedef enum
{
	static_comp,
	dynamic_comp
} computation_type_t;



/* sets the number of threads executing */
void
set_num_threads(unumber);

/* sets the type of computation execution */
void
set_computation_type(computation_type_t);

/* creates an mpi data type for the grid */
void
create_mpi_mb_grid_type();

/* destroys the mpi data type for the grid */
void
destroy_mpi_mb_grid_type();

/* creates an output struct */
struct part_output_grid*
create_output_grid(byte*, number);

/* destroys an output struct */
void
destroy_output_grid(struct part_output_grid*);

/* creates a mb_grid structure */
struct mb_grid* 
create_mb_grid(real,real,real,real,unumber,unumber);

/* computes a mandelbrot histogram for a set of points */
void
compute_mandelbrot (byte*, unumber*, struct mb_grid*);

/* calculates the mandelbrot set for the given grid */
void
calc_mbrot(struct mb_grid*);

/* calculates the number of iterations for a point */
void
compute_omp_dynamic(byte *buffer, unumber*, struct mb_grid* grid);

/* calculates the number of iterations for a point */
void
compute_omp_static(byte *buffer, unumber*, struct mb_grid* grid);

/* calculates the number of iterations for a point */
unumber
compute_point(real ci, real cr);

/* shows the values of a mb_grid */
void
show_mb_grid(struct mb_grid*);

/* deallocates a mb_grid struct */
void
destroy_mb_grid(struct mb_grid*);

#endif /* __MANDELBROT__ */