#include "stack.h"

#ifdef __STACK__

/* creates a stack */
struct stack*
create_stack()
{
	struct stack* s = (struct stack*) malloc(sizeof(struct stack));
	s->head = NULL;
  pthread_mutex_init(&s->sl, NULL);
	return s;
}

/* destroys a stack */
void
destroy_stack(struct stack* s)
{
	struct node* curr = s->head;
	if(curr)
	{
		while(curr->next != NULL)
		{
			struct node* tmp = curr->next;
			free(curr);
			curr = tmp;
		}
		free(curr);
	}
	free(s);
}

void
push(struct stack* s, void* data)
{
	struct node* new = (struct node*) malloc(sizeof(struct node));
	new->data = data;

	pthread_mutex_lock(&s->sl);
	new->next = s->head;
	s->head = new;
	pthread_mutex_unlock(&s->sl);
}

boolean
pop(struct stack* s, void** data)
{
	if(s->head == NULL)
	{
		return false;
	}
	pthread_mutex_lock(&s->sl);
	*data = s->head->data;
	struct node* old = s->head;
	s->head = s->head->next;
	pthread_mutex_unlock(&s->sl);
	
	free(old);
	return true;
}

#endif /* __STACK__ */